package main

import (
	"fmt"
	"os"
	"sync"

	"github.com/leaanthony/clir"
	"gitlab.com/poldi1405/go-ansi"
)

var (
	version = "unknown version"

	invert   = false
	failfast = false
	gitmode  = false
	plain    = false
	verbose  = false

	msgPrinter chan string
	nilPrinter chan string
	exit       bool
	wg         sync.WaitGroup
)

func main() {
	cli := clir.NewCli("isbin", "Check if a file is binary.", version)

	// define the status flag
	cli.BoolFlag("invert", "invert the logic (check if a file is ascii)", &invert)

	// define the faillfast flag
	cli.BoolFlag("failfast", "fail as soon as the status can be set to false", &failfast)

	// define the git-mode flag
	cli.BoolFlag("git-mode", "only check files if they are not ignored by git", &gitmode)

	// define the plain flag
	cli.BoolFlag("plain", "don't print explanatory text or colors", &plain)

	// define the verbose flag
	cli.BoolFlag("verbose", "print the result for every file", &plain)

	cli.Action(func() error {

		if invert {
			textFormatter = ansi.Green
			binaryFormatter = ansi.Red
		} else {
			binaryFormatter = ansi.Green
			textFormatter = ansi.Red
		}

		msgPrinter = make(chan string)
		nilPrinter = make(chan string)
		go printMessages()

		for _, file := range cli.OtherArgs() {
			go checkFile(file)
			wg.Add(1)
		}

		if failfast {
			go exitOnError()
		}
		wg.Wait()

		os.Exit(exitError)
		return nil
	})

	if err := cli.Run(); err != nil {
		fmt.Printf("Error encountered: %v\n", err)
	}
}

func printMessages() {
	for !exit {
		select {
		case str := <-msgPrinter:
			fmt.Println(str)
		default:
			break
		}
	}
	os.Exit(exitError)
}
