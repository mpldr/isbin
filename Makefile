GOFILES := $(shell go list -f {{.GoFiles}} | sed 's/\[\|\]//g')
VERSION := $(shell git describe --always --long --dirty)
GOOS := $(shell go env GOOS)
GOARCH := $(shell go env GOARCH)
GOEXT := $(shell go env GOEXT)

build:
	mkdir build
	go build -ldflags="-s -w -X main.buildVersion=${VERSION}" -trimpath -buildmode=pie -o build/${GOOS}_${GOARCH}/isbin${GOEXT}

run:
	go run ${GOFILES}

clean:
	rm build
