package detector

import (
	"io"
	"os"
)

func IsBin(path string) (bool, error) {
	file, err := os.Open(path)
	if err != nil {
		return false, err
	}
	defer file.Close()

	buf := make([]byte, 4*1024) // define your buffer size here.

	for {
		n, err := file.Read(buf)

		if n > 0 {
			if containsBin(buf[:n]) {
				return true, nil
			} // your read buffer.
		}

		if err == io.EOF {
			break
		}
		if err != nil {
			return false, err
		}
	}
	return false, nil
}

func containsBin(bytes []byte) bool {
	for _, b := range bytes {
		if _, ok := lowByteExceptions[int(b)]; ok {
			// these are CR and LF
			continue
		}
		if b < 32 || b == 127 {
			return true
		}
	}
	return false
}

var lowByteExceptions = map[int]bool{9: true, 10: true, 13: true}
