package main

var exitError int

func exitOnError() {
	for {
		if exitError != 0 {
			filetype := "textfile"

			if invert {
				filetype = "binary file"
			}

			if !plain {
				msgPrinter <- filetype + " found… exiting."
			}
			msgPrinter = nilPrinter
			exit = true
		}
	}
}
